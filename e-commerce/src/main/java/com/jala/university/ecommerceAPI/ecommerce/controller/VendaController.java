package com.jala.university.ecommerceAPI.ecommerce.controller;

import com.jala.university.ecommerceAPI.ecommerce.model.Venda;
import com.jala.university.ecommerceAPI.ecommerce.service.VendaService;
import com.jala.university.ecommerceAPI.ecommerce.utils.NotEnoughStockException;
import com.jala.university.ecommerceAPI.ecommerce.utils.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class VendaController {
    @Autowired
    private VendaService vendaService;

    @GetMapping("/vendas")
    public List<Venda> getAllVendas() {
        return vendaService.getAllVendas();
    }

    @PostMapping("/vendas")
    public ResponseEntity<String> realizarVenda(@RequestParam Long idProduto, @RequestParam int quantidade) {
        try {
            vendaService.realizarVenda(idProduto, quantidade);
            return ResponseEntity.ok("Venda realizada com sucesso");
        } catch (NotEnoughStockException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
        }
    }
}
