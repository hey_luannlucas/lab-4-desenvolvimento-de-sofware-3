package com.jala.university.ecommerceAPI.ecommerce.service;

import com.jala.university.ecommerceAPI.ecommerce.model.Produto;
import com.jala.university.ecommerceAPI.ecommerce.model.Venda;
import com.jala.university.ecommerceAPI.ecommerce.repository.ProdutoRepository;
import com.jala.university.ecommerceAPI.ecommerce.repository.VendaRepository;
import com.jala.university.ecommerceAPI.ecommerce.utils.NotEnoughStockException;
import com.jala.university.ecommerceAPI.ecommerce.utils.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class VendaService {
    @Autowired
    private VendaRepository vendaRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public List<Venda> getAllVendas() {
        return vendaRepository.findAll();
    }

    public void realizarVenda(Long idProduto, int quantidade) {
        if (quantidade <= 0) {
            throw new IllegalArgumentException("A quantidade vendida deve ser maior que zero");
        }

        Produto produto = produtoRepository.findById(idProduto)
                .orElseThrow(() -> new ResourceNotFoundException("Produto não encontrado"));

        if (produto.getEstoque() < quantidade) {
            throw new NotEnoughStockException("Estoque insuficiente para realizar esta venda. Estoque atual: " + produto.getEstoque());
        }

        double desconto = calcularDesconto(quantidade);

        double valorDesconto = 0.0;
        if (desconto > 0) {
            valorDesconto = produto.getPreco() * quantidade * desconto;
        }

        double valorTotal = produto.getPreco() * quantidade - valorDesconto;

        produto.setEstoque(produto.getEstoque() - quantidade);
        produtoRepository.save(produto);

        Venda venda = new Venda();
        venda.setProduto(produto);
        venda.setQuantidade(quantidade);
        venda.setData(LocalDate.now());
        venda.setComDesconto(desconto > 0);
        venda.setValorDesconto(valorDesconto);
        venda.setValorTotal(valorTotal);
        vendaRepository.save(venda);
    }

    private double calcularDesconto(int quantidade) {
        double desconto = 0.0;

        if (quantidade > 10 && quantidade <= 20) {
            desconto = 0.05; // 5% de desconto para mais de 10 unidades
        } else if (quantidade > 20) {
            desconto = 0.1; // 10% de desconto para mais de 20 unidades
        }

        return desconto;
    }
}
