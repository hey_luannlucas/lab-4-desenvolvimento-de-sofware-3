package com.jala.university.ecommerceAPI.ecommerce.repository;

import com.jala.university.ecommerceAPI.ecommerce.model.Venda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendaRepository extends JpaRepository<Venda, Long> {
}
