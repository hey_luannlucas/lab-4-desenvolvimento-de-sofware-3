package com.jala.university.ecommerceAPI.ecommerce.repository;

import com.jala.university.ecommerceAPI.ecommerce.model.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {
}

