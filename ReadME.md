# E-commerce API com Spring Boot 💻🛒

Este projeto é uma API de e-commerce desenvolvida utilizando o framework Spring Boot. Ela oferece funcionalidades básicas para gerenciar produtos e vendas em uma loja online. Abaixo, você encontrará uma descrição detalhada das principais características, tecnologias utilizadas e instruções para executar o projeto.

## Funcionalidades 🚀

- Obter todos os produtos
- Obter um produto por ID
- Criar um novo produto
- Atualizar um produto existente
- Excluir um produto
- Realizar uma venda
- Verificar historico 


## Rotas 📂

### Produtos

- `GET /api/produtos`: Retorna todos os produtos.
- `GET /api/produtos/{id}`: Retorna um produto com o ID especificado.
- `POST /api/produtos`: Cria um novo produto.
- `PUT /api/produtos/{id}`: Atualiza um produto existente com o ID especificado.
- `DELETE /api/produtos/{id}`: Exclui um produto com o ID especificado.

### Vendas

- `GET /api/vendas`: Retorna todas as vendas.
- `POST /api/vendas`: Realiza uma nova venda.

![](Assets/img.png)

## Tecnologias Utilizadas ⚙️

- **Spring Boot com java 17**: Framework de desenvolvimento para aplicativos Java.
- **Spring Data JPA**: Facilita o acesso e manipulação de dados em bancos de dados relacionais.
- **MySQL**: Sistema de gerenciamento de banco de dados relacional.
- **Swagger**: Ferramenta para documentação e exposição de APIs REST.
- **Docker**: Plataforma para desenvolvimento, implantação e execução de aplicativos em contêineres.

## Estrutura do Projeto 🏗️

O projeto está estruturado seguindo o padrão de arquitetura de software MVC (Model-View-Controller), com os pacotes organizados de forma a separar as responsabilidades de cada componente:

- **`com.jala.university.ecommerceAPI.ecommerce.controller`**: Contém as classes de controladores responsáveis por lidar com as requisições HTTP, separando a lógica de negócio das camadas de apresentação.
- **`com.jala.university.ecommerceAPI.ecommerce.model`**: Define as entidades de negócio da aplicação, separando a representação dos dados das regras de negócio.
- **`com.jala.university.ecommerceAPI.ecommerce.service`**: Contém as classes de serviço responsáveis por implementar a lógica de negócio da aplicação, isolando-a das camadas de apresentação e persistência.
- **`com.jala.university.ecommerceAPI.ecommerce.repository`**: Define interfaces que estendem JpaRepository para interagir com o banco de dados, mantendo a separação de responsabilidades entre as operações de banco de dados e a lógica de negócio.

Essa estrutura permite uma organização clara e modular do código, facilitando o desenvolvimento, manutenção e escalabilidade da aplicação.

## Executando o Projeto 🚀

Para executar este projeto localmente, você precisará ter o Docker instalado e em execução. Certifique-se de que o Docker está ativo antes de iniciar a aplicação.

Ao iniciar a aplicação, o Docker irá rodar e garantir que a infraestrutura necessária, como o banco de dados MySQL, esteja configurada corretamente. Além disso, será executado um script de inicialização que adiciona produtos à base de dados na primeira execução. Isso é feito para facilitar a manipulação da API e garantir que haja dados disponíveis para testes e desenvolvimento.

## Documentação da API 📃

A documentação da API pode ser acessada através do Swagger UI. Após iniciar a aplicação, acesse o seguinte URL em um navegador:

[Swagger UI](http://localhost:8080/swagger-ui/index.html) - http://localhost:8080/swagger-ui/index.html

Lá, você encontrará uma documentação interativa com todas as rotas disponíveis, parâmetros necessários e exemplos de resposta. Isso facilita a exploração e teste da API.


## Licença 📝
![Bower](https://img.shields.io/bower/l/MI?style=for-the-badge)
